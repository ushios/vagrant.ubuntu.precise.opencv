# OpenCV on Ubuntu12.04 precise
----
Version 0.01
Release date: Show commit log.
----
Project state:
beta
----
# Get started!

## Add precise64 box 

```
#!bash

$ vagrant box add precise64 http://files.vagrantup.com/precise64.box

```

## Vagrant up 

```
#!bash

$ vagrant up

```

----
# Credits

----
# Project description

----
# Dependencies

----
# Documentation

----
# Installation instructions

----
# Additional Notes




