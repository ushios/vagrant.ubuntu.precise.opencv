$opencv = Proc.new{|conf|
	conf.vm.box = $boxes[:precise64]

	conf.vm.network :private_network, ip: "192.168.100.12"
	conf.vm.network :forwarded_port, guest:80, host:8080

	# share folder
	conf.vm.synced_folder(
		$share_folders[:opencv],
		"/tmp/test_app",
		:owner => 'vagrant', :group => 'vagrant'
	)

	conf.vm.provider "virtualbox" do |v|
		v.customize ["modifyvm", :id, "--memory", "1024"]
		v.gui = true
	end
	
	conf.vm.provision :chef_solo do |chef|
		chef.cookbooks_path = $cookbooks_path
		chef.roles_path = $roles_path
		
		# add recipes (cookbooks folder)
		chef.add_recipe 'vim'
		chef.add_recipe 'apt::update_once'
		
		# add rolse (roles folder)		
		chef.add_role 'opencv'
		
		chef.json = {}
	end
}

